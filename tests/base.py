#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of testPipDependency.
# https://github.com/someuser/somepackage

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2015, Jose Luis Bellod Cisneros <bellod.cisneros@gmail.com>

from unittest import TestCase as PythonTestCase


class TestCase(PythonTestCase):
    pass
